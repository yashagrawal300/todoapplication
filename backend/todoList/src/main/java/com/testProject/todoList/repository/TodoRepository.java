package com.testProject.todoList.repository;

import com.testProject.todoList.model.TodoModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TodoRepository extends JpaRepository<TodoModel, String> {

    void deleteAlByTodoid(int todoid);

    TodoModel getByTodoid(int todoid);

    List<TodoModel> findAllByLoginid(String loginid);
}
