package com.testProject.todoList.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "todotable")
public class TodoModel {
    @Id
    private int todoid;
    private String todo;
    private String loginid;
    private boolean completed;

    public TodoModel(){

    }

    public TodoModel(String todo, String loginid, int todoid, boolean completed) {
        this.todo = todo;
        this.loginid = loginid;
        this.todoid = todoid;
        this.completed = completed;
    }

    public String getTodo() {
        return todo;
    }

    public void setTodo(String todo) {
        this.todo = todo;
    }

    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    public int getTodoid() {
        return todoid;
    }

    public void setTodoid(int todoid) {
        this.todoid = todoid;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
