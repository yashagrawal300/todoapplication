package com.testProject.todoList.service;

import com.testProject.todoList.model.Usermodel;
import com.testProject.todoList.repository.UserRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Userservice {
    @Autowired
    UserRepository userRepository;

    public String addUser(Usermodel usermodel){
        int x = (int)userRepository.count();
        usermodel.setLoginid(usermodel.getUsername() + (x+1));
        userRepository.save(usermodel);
        return "added user";

    }

    public List<String> getAllUsername(){
        return userRepository.getAllUsername();
    }




}
