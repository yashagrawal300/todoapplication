package com.testProject.todoList.controller;

import com.testProject.todoList.model.Usermodel;
import com.testProject.todoList.service.Userservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {
    @Autowired
    Userservice userservice;

    @GetMapping("/api/getAllUsers")
    public List<String> getAllUsers(){
        return userservice.getAllUsername();

    }

    @PostMapping("/api/addUser")
    public String addUser(@RequestBody Usermodel usermodel){
        return userservice.addUser(usermodel);

    }
}
