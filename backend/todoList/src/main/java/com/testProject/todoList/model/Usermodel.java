package com.testProject.todoList.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "usertable")
public class Usermodel {
    @Id
    private String loginid;
    private String username;
    private String password;

    public Usermodel(){

    }

    public Usermodel(String loginid, String username, String password, String authkey) {
        this.loginid = loginid;
        this.username = username;
        this.password = password;

    }

    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}

