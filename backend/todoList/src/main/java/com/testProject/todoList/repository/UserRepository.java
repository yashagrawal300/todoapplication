package com.testProject.todoList.repository;

import com.testProject.todoList.model.Usermodel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserRepository extends JpaRepository<Usermodel, String> {
    @Query(value = "SELECT username from users.usertable", nativeQuery = true)
    List<String> getAllUsername();
}
