package com.testProject.todoList.service;

import com.testProject.todoList.model.TodoModel;
import com.testProject.todoList.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class TodoService {
    @Autowired
    TodoRepository todoRepository;


    public List<TodoModel> getAllTodos(String loginid){
        return todoRepository.findAllByLoginid(loginid);
    }


    public String addTodos(TodoModel todoModel){
        int count = (int) todoRepository.count();
        todoModel.setTodoid(count+1);
        todoRepository.save(todoModel);
        return "added";
    }

    public String deleteTodoBytodoid(int todoid){
        todoRepository.deleteAlByTodoid(todoid);
        return "deleted";
    }


    public String updateCompletedById(int todoid) {
        TodoModel todoModel = todoRepository.getByTodoid(todoid);
        todoModel.setCompleted(!todoModel.isCompleted());
        todoRepository.save(todoModel);
        return "updated";

    }
}
