package com.testProject.todoList.controller;

import com.testProject.todoList.model.TodoModel;
import com.testProject.todoList.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TodoController {
    @Autowired
    TodoService todoService;

    @GetMapping("/api/getTodoById/{loginId}")
    public List<TodoModel> getTodoById(@PathVariable String loginId){
        return todoService.getAllTodos(loginId);
    }

    @Transactional
    @GetMapping("/api/deleteById/{todoid}")
    public String deleteById(@PathVariable int todoid){
        return todoService.deleteTodoBytodoid(todoid);
    }

    @GetMapping("/api/updatingCompletedById/{todoid}")
    public String updateCompletedById(@PathVariable int todoid){
        return todoService.updateCompletedById(todoid);
    }


    @PostMapping("/api/addData")
    public String addData(@RequestBody TodoModel todoModel){
        return todoService.addTodos(todoModel);
    }



}
